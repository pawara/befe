import $ from "jquery";
import React from 'react';
import {Router, Route, Switch} from 'react-router-dom'
import './App.css';

import HeaderComponent from './components/Header/HeaderComponent';
import FooterComponent from './components/Footer/FooterComponent';
import Home from './components/Home/Home';
import About from './components/About/About';
import Contact from './components/Contact/Contact';

import Vehicles from './components/Services/Vehicles/Vehicles';
import ViewVehicleItem from './components/Services/Vehicles/ViewVehicleItem';

import Spareparts from './components/Services/Spareparts/Spareparts';
import ViewSparepartItem from './components/Services/Spareparts/ViewSparepartItem';

import Keys from './components/Services/Keys/Keys';
import ViewKeyItem from './components/Services/Keys/ViewKeyItem';

import AdminKeyDashboard from './components/Admin/Keys/AdminKeyDashboard';
import CreateKeyComponent from './components/Admin/Keys/CreateKeyComponent';
import ViewKeyComponent from './components/Admin/Keys/ViewKeyComponent';

import AdminVehicleDashboard from './components/Admin/Vehicles/AdminVehicleDashboard';
import CreateVehicleComponent from './components/Admin/Vehicles/CreateVehicleComponent';
import ViewVehicleComponent from './components/Admin/Vehicles/ViewVehicleComponent';

import AdminSparepartDashboard from './components/Admin/Spareparts/AdminSparepartDashboard';
import CreateSparepartComponent from './components/Admin/Spareparts/CreateSparepartComponent';
import ViewSparepartComponent from './components/Admin/Spareparts/ViewSparepartComponent';
import history from './history';

function App() {
  return (
    <div className="App">
      <Router history={history}>
      <div>
        <HeaderComponent/>
                  <div className="container">
                
                    <Switch>
                        <Route path="/" exact component={Home}></Route>
                        <Route path="/about" exact component={About}></Route>
                        <Route path="/contact" exact component={Contact}></Route>

                        <Route path="/vehicles" exact component={Vehicles}></Route>
                        <Route path="/view-vehicle/:id" exact component={ViewVehicleItem}></Route>
                        <Route path="/admin/vehicles" exact component={AdminVehicleDashboard}></Route>
                        <Route path="/admin/add-vehicles/:id" exact component={CreateVehicleComponent}></Route>
                        <Route path="/admin/view-vehicle/:id" exact component={ViewVehicleComponent}></Route>
                        
                        <Route path="/spareparts" exact component={Spareparts}></Route>
                        <Route path="/view-sparepart/:id" exact component={ViewSparepartItem}></Route>
                        <Route path="/admin/spareparts" exact component={AdminSparepartDashboard}></Route>
                        <Route path="/admin/add-spareparts/:id" exact component={CreateSparepartComponent}></Route>
                        <Route path="/admin/view-sparepart/:id" exact component={ViewSparepartComponent}></Route>
                        
                        <Route path="/keys" exact component={Keys}></Route>
                        <Route path="/view-key/:id" exact component={ViewKeyItem}></Route>
                        <Route path="/admin/keys" exact component={AdminKeyDashboard}></Route>
                        <Route path="/admin/add-keys/:id" exact component={CreateKeyComponent}></Route>
                        <Route path="/admin/view-key/:id" exact component={ViewKeyComponent}></Route>
                        
                        {/* <Route path="/employees" exact component={ListEmployeeComponent}></Route> */}

                        {/* step 1 */}
                        {/* <Route path="/add-employee/:id" exact component={CreateEmployeeComponent}></Route> */}
                        {/* <Route path="/update-employee/:id" exact component={UpdateEmployeeComponent}></Route> */}
                    
                        {/* <Route path="/view-employee/:id" exact component={ViewEmployeeComponent}></Route> */}
                    
                    </Switch>
                  </div>
        <FooterComponent/>
        </div>
      </Router>
    </div>
  );
}

export default App;
