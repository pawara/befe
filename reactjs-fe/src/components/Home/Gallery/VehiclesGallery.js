import React, { Component } from 'react';
// import "tabler-react/dist/Tabler.css";
// import { GalleryCard, Grid, Card } from 'tabler-react';

import { Button, Form, FormControl, NavDropdown, Nav, Navbar  } from 'react-bootstrap';
import Banner1 from "./../resources/Banner1.jpg";
import './Gallery.css';
import VehicleService from '../../../api-request-general/VehicleService';
import history from '../../../history';

class VehiclesGallery extends Component {

    constructor(props){
        super(props);

        this.state={
            vehicles: []
        }

        this.viewVehicle=this.viewVehicle.bind(this);
    }

    viewVehicle(id)
    {
        history.push(`/view-vehicle/${id}`);
        
    }

    componentDidMount(){
        VehicleService.getVehicles().then((res) =>{
            this.setState({vehicles:res.data.sort(function (a, b) { return 0.5 - Math.random() }).slice(0, 4)});
        });
    }

    render() {
        return (
            
                
                <div className="card-group">
                    <div class="row">
                    {
                        this.state.vehicles.map(
                            vehicle =>
                            <div class="col-3">
                                <div class="card">
                                    <img class="card-img-top" src={`data:image/jpeg;base64,${vehicle.image}`}  alt="Card image cap"/>
                                    <div class="card-body">
                                    <h5 class="card-title"><div>{vehicle.productCode}</div></h5>
                                    <p class="card-text">
                                        <label>Vehicle Brand: </label>
                                        <div>{vehicle.vehicleBrand}</div>
                                    </p>
                                    <p class="card-text">
                                        
                                        Vehicle Type: {vehicle.vehicleType}
                                    </p>
                                    <p class="card-text">
                                        <label><div>Price: {vehicle.price}</div></label>
                                        
                                    </p>
                                    <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                                    <div class="btn-group" style={{width:"100%"}}>
                                    {/* <button  type="button"  onClick={() => this.viewVehicle(vehicle.id)} class="btn btn-info" >View More</button > */}
                                    </div>
                                    </div>
                                </div>
                                </div>
                        )
                    }
                    </div>
            </div>
            
        );
    }
}

export default VehiclesGallery;

