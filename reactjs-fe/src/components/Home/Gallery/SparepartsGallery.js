import React, { Component } from 'react';
// import "tabler-react/dist/Tabler.css";
// import { GalleryCard, Grid, Card } from 'tabler-react';

import { Button, Form, FormControl, NavDropdown, Nav, Navbar  } from 'react-bootstrap';
import Banner1 from "./../resources/Banner1.jpg";
import './Gallery.css';
import SparepartService from '../../../api-request-general/SparepartService';
import history from '../../../history';

class SparepartsGallery extends Component {

    constructor(props){
        super(props);

        this.state={
            spareparts: []
        }

        // this.viewKey=this.viewKey.bind(this);
    }

    // viewKey(id)
    // {
    //     history.push(`/view-sparepart/${id}`);
        
    // }

    componentDidMount(){
        SparepartService.getSpareparts().then((res) =>{
            this.setState({spareparts:res.data.sort(function (a, b) { return 0.5 - Math.random() }).slice(0, 4)});
        });
    }

    render() {
        return (
            
                
                <div className="card-group">
                    <div class="row">
                    {
                        this.state.spareparts.map(
                            spareparts =>
                            <div class="col-3">
                                <div class="card">
                                    <img class="card-img-top" src={`data:image/jpeg;base64,${spareparts.image}`}  alt="Card image cap"/>
                                    <div class="card-body">
                                    <h5 class="card-title"><div>{spareparts.productCode}</div></h5>
                                    <p class="card-text">
                                        <label>Vehicle Brand: </label>
                                        <div>{spareparts.vehicleBrand}</div>
                                    </p>
                                    <p class="card-text">
                                        
                                        Vehicle Type: {spareparts.vehicleType}
                                    </p>
                                    <p class="card-text">
                                        
                                        Sparepart Name: {spareparts.sparepartName}
                                    </p>
                                    <p class="card-text">
                                        <label><div>Price: {spareparts.price}</div></label>
                                        
                                    </p>
                                    {/* <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p> */}
                                    <div class="btn-group" style={{width:"100%"}}>
                                    {/* <button  type="button"  onClick={() => this.viewKey(key.id)} class="btn btn-info" >View More</button > */}
                                    </div>
                                    </div>
                                </div>
                                </div>
                        )
                    }
                    </div>
            </div>
            
        );
    }
}

export default SparepartsGallery;

