import React, { Component } from 'react';

import Gallery from './Gallery/Gallery';
import SparepartsGallery from './Gallery/SparepartsGallery';
import VehiclesGallery from './Gallery/VehiclesGallery';
import Corousel from './Corousel/Corousel';

import history from '../../history';
import './Home.css';
class Home extends Component {

    constructor(props){
        super(props);

        this.state={
            
        }
    }

    viewVehicle()
    {
        history.push(`/vehicles/`);
    }

    viewKey()
    {
        history.push(`/keys/`);
    }

    viewSpareparts()
    {
        history.push(`/spareparts/`);
    }
    
    render() {
        return (
            <div>
                {/* <div><br/></div> */}
                <Corousel/>
                <div><br/>Vehicle Keys</div>
                <Gallery/><div><br/></div>
                {/* <div>View More Keys</div> */}
                <button  type="button"  onClick={() => this.viewKey()} class="block" >View More Keys</button >
                <div><br/>Spareparts</div>
                <SparepartsGallery/><div><br/></div>
                {/* <div >View More Spareparts</div> */}
                <div>
                <button  type="button"  onClick={() => this.viewSpareparts()} class="block" >View More Spareparts</button >
                </div><div><br/>Vehicles</div>
                <VehiclesGallery/><div><br/></div>
                {/* <div>View More Vehicles</div> */}
                <button  type="button"  onClick={() => this.viewVehicle()} class="block" >View More Vehicles</button >
                <div><br/></div>
            </div>
        );
    }
}

export default Home;