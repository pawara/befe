import React, { Component } from 'react';
import VehicleService from '../../../api-request/VehicleService';
import history from '../../../history';

class AdminVehicleDashboard extends Component {
    
    constructor(props){
        super(props)

        this.state={
            vehicles: []
        }
        this.addVehicle=this.addVehicle.bind(this); //need to bind relevant calling functions for btn events
        this.editVehicle=this.editVehicle.bind(this);

        //bind event handler
        this.deleteVehicle=this.deleteVehicle.bind(this);

        this.viewVehicle=this.viewVehicle.bind(this);
    }

    viewVehicle(id)
    {
        history.push(`/admin/view-vehicle/${id}`);
    }

    deleteVehicle(id){
        //rest api calling from here
        VehicleService.deleteVehicle(id).then((res) =>{
            //already deleted with deleteEmployee rest call.. now lets update in UI
            this.setState({vehicles: this.state.vehicles.filter(vehicle => vehicle.id !== id)});
        });
    }

    editVehicle(id){
        history.push(`/admin/add-vehicles/${id}`);
    }

    componentDidMount(){
        VehicleService.getVehicles().then((res) =>{
            this.setState({vehicles:res.data});
            console.log(" ------------------------------------------------- console.log(this.state.vehicles);");
            console.log(this.state.vehicles);
        });
        
    }

    addVehicle(){
        // this.props.history.push("/add-employee/-1")//This is how url received -1 
        history.push("/admin/add-vehicles/_add")//This is how url received -1 


        // React router basically maintains history object.. this has all the mappings of the routings
        // react router pass history obj to all router as props
    }

    render() {
        return (
            <div>
                <h2 className="text-center">Vehicles List</h2>
                <div className="row">
                    <button className="btn btn-primary" onClick={this.addVehicle}>Add Vehicle</button>
                </div>
                <div className="row">
                    <table className="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>Product Code</th>
                                <th>Vehicle Brand</th>
                                <th>Vehicle Type</th>
                                <th>Quantity</th>
                                <th>Price</th>
                                <th>Actions</th>
                            </tr>
                        </thead>

                        <tbody>
                            {
                                this.state.vehicles.map(
                                    vehicle =>
                                    <tr vehicle= {vehicle.id}>
                                        <td>{vehicle.productCode}</td>
                                        <td>{vehicle.vehicleBrand}</td>
                                        <td>{vehicle.vehicleType}</td>
                                        <td>{vehicle.quantity}</td>
                                        <td>{vehicle.price}</td>
 
                                        <td>
                                            <button onClick={() => this.editVehicle(vehicle.id)} className="btn btn-info">Update</button>
                                            <button style={{marginLeft:"10px"}} onClick={() => this.deleteVehicle(vehicle.id)} className="btn btn-danger">Delete</button>
                                            <button style={{marginLeft:"10px"}} onClick={() => this.viewVehicle(vehicle.id)} className="btn btn-info">View</button>
                                        </td>
                                    </tr>
                                )
                            }
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }
}

export default AdminVehicleDashboard;