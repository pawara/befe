import React, { Component } from 'react';
import KeyService from '../../../api-request/KeyService';
import history from '../../../history';

class AdminKeyDashboard extends Component {
    
    constructor(props){
        super(props)

        this.state={
            keys: []
        }
        this.addKey=this.addKey.bind(this); //need to bind relevant calling functions for btn events
        this.editKey=this.editKey.bind(this);

        //bind event handler
        this.deleteKey=this.deleteKey.bind(this);

        this.viewKey=this.viewKey.bind(this);
    }

    viewKey(id)
    {
        history.push(`/admin/view-key/${id}`);
    }

    deleteKey(id){
        //rest api calling from here
        KeyService.deleteKey(id).then((res) =>{
            //already deleted with deleteEmployee rest call.. now lets update in UI
            this.setState({keys: this.state.keys.filter(key => key.id !== id)});
        });
    }

    editKey(id){
        history.push(`/admin/add-keys/${id}`);
    }

    componentDidMount(){
        KeyService.getKeys().then((res) =>{
            this.setState({keys:res.data});
            console.log(" ------------------------------------------------- console.log(this.state.keys);");
            console.log(this.state.keys);
        });
        
    }

    addKey(){
        // this.props.history.push("/add-employee/-1")//This is how url received -1 
        history.push("/admin/add-keys/_add")//This is how url received -1 


        // React router basically maintains history object.. this has all the mappings of the routings
        // react router pass history obj to all router as props
    }

    render() {
        return (
            <div>
                <h2 className="text-center">Keys List</h2>
                <div className="row">
                    <button className="btn btn-primary" onClick={this.addKey}>Add Key</button>
                </div>
                <div className="row">
                    <table className="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>Product Code</th>
                                <th>Vehicle Brand</th>
                                <th>Vehicle Type</th>
                                <th>Quantity</th>
                                <th>Price</th>
                                <th>Actions</th>
                            </tr>
                        </thead>

                        <tbody>
                            {
                                this.state.keys.map(
                                    key =>
                                    <tr key= {key.id}>
                                        <td>{key.productCode}</td>
                                        <td>{key.vehicleBrand}</td>
                                        <td>{key.vehicleType}</td>
                                        <td>{key.quantity}</td>
                                        <td>{key.price}</td>
 
                                        <td>
                                            <button onClick={() => this.editKey(key.id)} className="btn btn-info">Update</button>
                                            <button style={{marginLeft:"10px"}} onClick={() => this.deleteKey(key.id)} className="btn btn-danger">Delete</button>
                                            <button style={{marginLeft:"10px"}} onClick={() => this.viewKey(key.id)} className="btn btn-info">View</button>
                                        </td>
                                    </tr>
                                )
                            }
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }
}

export default AdminKeyDashboard;