import React, { Component } from 'react';
import history from '../../../history';
import SparepartService from '../../../api-request/SparepartService';

class CreateSparepartComponent extends Component {

    constructor(props){
        super(props);
        console.log('qqqqqqqqqqqqq e0ccfcf0-ea7d-4050-8b3b-49241fc0e59b');
        this.state={
            id: this.props.match.params.id, //id get from route
            productCode:'',
            vehicleBrand:'',
            vehicleType:'',
            sparepartName:'',
            quantity: '',
            price: '',
            image:'',
            image_url: '',
        }
        // step2
        this.changeProductCodeHandler = this.changeProductCodeHandler.bind(this);
        this.changeVehicleBrandHandler = this.changeVehicleBrandHandler.bind(this);
        this.changeVehicleTypeHandler = this.changeVehicleTypeHandler.bind(this);
        this.changeQuantityHandler = this.changeQuantityHandler.bind(this);
        this.changePriceHandler = this.changePriceHandler.bind(this);
        this.changeSparepartNameHandler = this.changeSparepartNameHandler.bind(this);
        this.saveOrUpdateSparepart = this.saveOrUpdateSparepart.bind(this);

        //image file upload
        this.uploadSingleFile = this.uploadSingleFile.bind(this);
    }

    // Step 3 - get employee obj by id
    componentDidMount()
    {

        // Step 4
        console.log('123');
        if(this.state.id === '_add'){
            return
        }else{
            SparepartService.getSparepartById(this.state.id).then((res) => {
                let sparepart = res.data;
                this.setState({
                    productCode: sparepart.productCode,
                    vehicleBrand: sparepart.vehicleBrand,
                    vehicleType: sparepart.vehicleType,
                    quantity: sparepart.quantity,
                    price: sparepart.price,
                    image: sparepart.image,
                    sparepartName: sparepart.sparepartName,
                    // image_url: sparepart.image,
                    // imageProfile: employee.imageProfile,
                });
                // this.setState({image: event.target.files[0]});
                // this.setState({image_url: URL.createObjectURL(this.state.image)});
                // console.log('formData.get("data") =>' +res.data);
            });
        }
        
        // console.log('formData.get("firstName") =>' +formData.get('firstName'));
        // console.log('formData.get("lastName") =>' +formData.get('lastName'));
        // console.log('formData.get("emailId") =>' +formData.get('emailId'));
        // console.log('this.state.data2 =>' +this.state.data);
        // console.log('this.state.file2 =>' +this.state.file);
    }
    
    saveOrUpdateSparepart = (e) =>{
        e.preventDefault();

        let formData = new FormData();
        // formData.append('id', this.state.id);
        formData.append('image', this.state.image);
        formData.append('product_code', this.state.productCode);
        formData.append('vehicle_brand', this.state.vehicleBrand);
        formData.append('vehicle_type', this.state.vehicleType);
        formData.append('quantity', this.state.quantity);
        formData.append('price', this.state.price);
        formData.append('sparepart_name', this.state.sparepartName);
        console.log('formData.get("image") =>' +formData.get('image'));
        console.log('formData.get("firstName") =>' +formData.get('firstName'));
        // console.log('formData.get("lastName") =>' +formData.get('lastName'));
        // console.log('formData.get("emailId") =>' +formData.get('emailId'));
        // console.log('this.state.data2 =>' +this.state.data);
        // console.log('this.state.file2 =>' +this.state.file);
        // step 5
        if(this.state.id === '_add'){
            
            SparepartService.createSparepart(formData).then(res =>{
                history.push('/admin/spareparts')//handle navigation after successfull submission
                }
                );
        }
        else{
                    //we can use then, when calling method returns a promise object
            SparepartService.updateSparepart(formData, this.state.id).then( res => {
                history.push('/admin/spareparts');
            });
        }
    }
    //eventhandler to set firstname
    changeProductCodeHandler = (event) =>{
        this.setState({productCode: event.target.value});
    }

    changeVehicleBrandHandler = (event) =>{
        this.setState({vehicleBrand: event.target.value});
    }

    changeSparepartNameHandler = (event) =>{
        this.setState({sparepartName: event.target.value});
    }

    changeVehicleTypeHandler = (event) =>{
        this.setState({vehicleType: event.target.value});
    }
    changeQuantityHandler = (event) =>{
        this.setState({quantity: event.target.value});
    }
    changePriceHandler = (event) =>{
        this.setState({price: event.target.value});
    }
    cancel()
    {
        history.push("/admin/spareparts")
    }
    getTitle(){
        if(this.state.id=== '_add'){
            return <h3 className="text-center">Add Sparepart</h3>
        }else{
            return <h3 className="text-center">Update Sparepart</h3>
        }
    }

    uploadSingleFile = (event) =>{
        this.setState({image_url: URL.createObjectURL(event.target.files[0])});
        this.setState({image: event.target.files[0]});
        // this.setImage(event.target.files[0])
        // console.log('this.state.file =>' +this.state.file);
        // console.log('this.state.data =>' +this.state.data);
        // console.log('e.target.files[0] =>' +event.target.files[0]);
        // console.log('URL.createObjectURL(event.target.files[0]) =>' +URL.createObjectURL(event.target.files[0]));
        // console.log('image =>' +this.image);
        // this.image = event.target.files[0];
    }

    render() {
        let imgPreview;
        if (this.state.image_url) {
            // {this.state.file;}
            imgPreview = <img src={this.state.image_url} alt='' />;
        }

        return (
            <div>
                
                <div className="container">
                    <div className = "row">
                        <div className = "card col-md-6 offset-md-3 offset-md-3">
                            {
                                this.getTitle()
                            }
                            <div className="card-body">
                                <form>

                                    <div className = "form-group">
                                        <label>Product Code: </label>
                                        <input placeholder="Product Code" name="productCode" className="form-control"
                                            value={this.state.productCode} onChange={this.changeProductCodeHandler}/>
                                    </div>
                                    <div className = "form-group">
                                        <label>Vehicle Brand: </label>
                                        <input placeholder="Vehicle Brand" name="vehicleBrand" className="form-control"
                                            value={this.state.vehicleBrand} onChange={this.changeVehicleBrandHandler}/>
                                    </div>
                                    <div className = "form-group">
                                        <label>Vehicle Type: </label>
                                        <input placeholder="Vehicle Type" name="vehicleType" className="form-control"
                                            value={this.state.vehicleType} onChange={this.changeVehicleTypeHandler}/>
                                    </div>
                                    <div className = "form-group">
                                        <label>Sparepart Name: </label>
                                        <input placeholder="Sparepart Name" name="sparepartName" className="form-control"
                                            value={this.state.sparepartName} onChange={this.changeSparepartNameHandler}/>
                                    </div>
                                    <div className = "form-group">
                                        <label>Quantity: </label>
                                        <input placeholder="Quantity" name="quantity" className="form-control"
                                            value={this.state.quantity} onChange={this.changeQuantityHandler}/>
                                    </div>
                                    <div className = "form-group">
                                        <label>Price: </label>
                                        <input placeholder="Price" name="price" className="form-control"
                                            value={this.state.price} onChange={this.changePriceHandler}/>
                                    </div>

                                    <div className="form-group preview">
                                        {imgPreview}
                                    </div>

                                    <div className="form-group">
                                        <input type="file" className="form-control" onChange={this.uploadSingleFile} />
                                    </div>

                                    <button className="btn btn-success" onClick={this.saveOrUpdateSparepart}>Save</button>
                                    <button className="btn btn-danger" onClick={this.cancel.bind(this)} style={{marginLeft:"10px"}}>Cancel</button>
                                
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default CreateSparepartComponent;