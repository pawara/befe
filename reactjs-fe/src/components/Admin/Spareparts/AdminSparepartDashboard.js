import React, { Component } from 'react';
import SparepartService from '../../../api-request/SparepartService';
import history from '../../../history';

class AdminSparepartDashboard extends Component {
    
    constructor(props){
        super(props)

        this.state={
            spareparts: []
        }
        this.addSparepart=this.addSparepart.bind(this); //need to bind relevant calling functions for btn events
        this.editSparepart=this.editSparepart.bind(this);

        //bind event handler
        this.deleteSparepart=this.deleteSparepart.bind(this);

        this.viewSparepart=this.viewSparepart.bind(this);
    }

    viewSparepart(id)
    {
        history.push(`/admin/view-sparepart/${id}`);
    }

    deleteSparepart(id){
        //rest api calling from here
        SparepartService.deleteSparepart(id).then((res) =>{
            //already deleted with deleteEmployee rest call.. now lets update in UI
            this.setState({spareparts: this.state.spareparts.filter(sparepart => sparepart.id !== id)});
        });
    }

    editSparepart(id){
        history.push(`/admin/add-spareparts/${id}`);
    }

    componentDidMount(){
        SparepartService.getSpareparts().then((res) =>{
            this.setState({spareparts:res.data});
            console.log(" ------------------------------------------------- console.log(this.state.spareparts);");
            console.log(this.state.spareparts);
        });
        
    }

    addSparepart(){
        // this.props.history.push("/add-employee/-1")//This is how url received -1 
        history.push("/admin/add-spareparts/_add")//This is how url received -1 


        // React router basically maintains history object.. this has all the mappings of the routings
        // react router pass history obj to all router as props
    }

    render() {
        return (
            <div>
                <h2 className="text-center">Spareparts List</h2>
                <div className="row">
                    <button className="btn btn-primary" onClick={this.addSparepart}>Add Sparepart</button>
                </div>
                <div className="row">
                    <table className="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>Product Code</th>
                                <th>Vehicle Brand</th>
                                <th>Vehicle Type</th>
                                <th>Quantity</th>
                                <th>Price</th>
                                <th>Actions</th>
                            </tr>
                        </thead>

                        <tbody>
                            {
                                this.state.spareparts.map(
                                    sparepart =>
                                    <tr sparepart= {sparepart.id}>
                                        <td>{sparepart.productCode}</td>
                                        <td>{sparepart.vehicleBrand}</td>
                                        <td>{sparepart.vehicleType}</td>
                                        <td>{sparepart.quantity}</td>
                                        <td>{sparepart.price}</td>
 
                                        <td>
                                            <button onClick={() => this.editSparepart(sparepart.id)} className="btn btn-info">Update</button>
                                            <button style={{marginLeft:"10px"}} onClick={() => this.deleteSparepart(sparepart.id)} className="btn btn-danger">Delete</button>
                                            <button style={{marginLeft:"10px"}} onClick={() => this.viewSparepart(sparepart.id)} className="btn btn-info">View</button>
                                        </td>
                                    </tr>
                                )
                            }
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }
}

export default AdminSparepartDashboard;