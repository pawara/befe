import React, { Component } from 'react';
import './Footer.css';

import imag from "./resources/logo_v1.png";
import history from '../../history';

class Footer extends Component {

    constructor(props) {
        super(props);

        this.state = {

        }

        this.about = this.about.bind(this);
        this.vehicles = this.vehicles.bind(this);
        this.contact = this.contact.bind(this);
        this.keys = this.keys.bind(this);
        this.spareparts = this.spareparts.bind(this);
    }

    about() {
        // this.props.history.push('/about');
        history.push('/about');
    }
    vehicles() {
        // this.props.history.push('/services');
        history.push('/vehicles');
    }
    contact() {
        // this.props.history.push('/contact');
        history.push('/contact');
    }
    keys() {
        // this.props.history.push('/keys');
        history.push('/keys');
    }
    spareparts() {
        // this.props.history.push('/keys');
        history.push('/spareparts');
    }

    render() {
        return (
            // <div>
            <div >
                <section style={{ height: "80px" }}></section>
                <div class="row" style={{ textalign: "center" }}>
                    {/* <h2>Bootstrap Dark Footer UI</h2> */}
                </div>

                <footer class="footer-bs">
                    <div class="row">
                        <div class="col-md-4 footer-brand animated fadeInLeft">
                        <img src={imag} alt="store" className="navbar-brand-2" />
                            <p>We are front line company which providing vehicle related services to our beloved customeras.
                            By shopping with us, you can buy your:
                                    <li ><a onClick={() => this.keys()}>Smart Vehicle Keys</a></li>
                                <li><a onClick={() => this.spareparts()}>Spareparts</a></li>
                                <li><a onClick={() => this.vehicles()}>Vehicles</a></li>
                            </p>
                            <p>© 2020 Aztech, All rights reserved</p>
                        </div>
                        <div class="col-md-4 footer-nav animated fadeInUp">
                            <h4>Contact Information</h4>
                            {/* <div class="col-md-4"> */}
                                <ul class="pages">
                                <li class="list-item"> <i class="fa fa-map-marker"> </i> Melbourne, Australia</li>
                                <li class="list-item"> <i class="fa fa-phone"> 0406765601</i></li>
                                <li class="list-item"> <i class="fa fa-envelope"> aussl@bigpond.com</i></li>
                                {/* <li class="list-item"> <i class="fa fa-map-marker"> Melbourne, Australia</i></li> */}
                                    {/* <li><a href="#">Tel:</a></li> */}
                                    {/* <li><a href="#">Email:</a></li> */}
                                    <li><a href="#">Web: www.sus-sl.com</a></li>
                                </ul>
                            {/* </div> */}
                            {/* <div class="col-md-4"> */}
                                <ul class="list">
                                    <li><a onClick={() => this.about()}>About Us</a></li>
                                    <li><a onClick={() => this.contact()}>Contacts</a></li>

                                </ul>
                            {/* </div> */}
                        </div>
                        <div class="col-md-4 footer-social animated fadeInDown">
                            <h4>Follow Us</h4>
                            <ul>
       
                                <li class="list-inline-item"><a ><i class="fa fa-facebook"></i></a></li>
                                <li class="list-inline-item"><a ><i class="fa fa-twitter"></i></a></li>
                                <li class="list-inline-item"><a href="https://www.fiverr.com/share/qb8D02"><i class="fa fa-google-plus"></i></a></li>
                                <li class="list-inline-item"><a href="https://www.fiverr.com/share/qb8D02" target="_blank"><i class="fa fa-envelope"></i></a></li>
                                <li class="list-inline-item"><a href="https://www.fiverr.com/share/qb8D02" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                            
                            </ul>
                        </div>



                    </div>
                </footer>
                <section style={{ textalign: "center", margin: "10px auto" }}><p>@Designed by <a >Aztech</a></p></section>

            </div>
            // </div>
        );
    }
}

export default Footer;