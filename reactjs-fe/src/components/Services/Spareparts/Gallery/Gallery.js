import React, { Component } from 'react';
// import "tabler-react/dist/Tabler.css";
// import { GalleryCard, Grid, Card } from 'tabler-react';

import { Button, Form, FormControl, NavDropdown, Nav, Navbar  } from 'react-bootstrap';

import './Gallery.css';
import SparepartService from '../../../../api-request-general/SparepartService';
import history from '../../../../history';

class Gallery extends Component {

    constructor(props){
        super(props);

        this.state={
            spareparts: []
        }

        this.viewSparepart=this.viewSparepart.bind(this);
    }

    viewSparepart(id)
    {
        history.push(`/view-sparepart/${id}`);
    }

    componentDidMount(){
        SparepartService.getSpareparts().then((res) =>{
            this.setState({spareparts:res.data});
        });
    }

    render() {
        return (
            
                
                <div className="card-group">
                    <div class="row">
                    {
                        this.state.spareparts.map(
                            sparepart =>
                            <div class="col-3">
                                <div class="card">
                                    <img class="card-img-top" src={`data:image/jpeg;base64,${sparepart.image}`}  alt="Card image cap"/>
                                    <div class="card-body">
                                    <h5 class="card-title"><div>{sparepart.productCode}</div></h5>
                                    <p class="card-text">
                                        <label>Vehicle Brand: </label>
                                        <div>{sparepart.vehicleBrand}</div>
                                    </p>
                                    <p class="card-text">
                                        
                                        Vehicle Type: {sparepart.vehicleType}
                                    </p>
                                    <p class="card-text">
                                        
                                        Sparepart Name: {sparepart.sparepartName}
                                    </p>
                                    <p class="card-text">
                                        <label><div>Price: {sparepart.price}</div></label>
                                        
                                    </p>
                                    <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                                    <div class="btn-group" style={{width:"100%"}}>
                                    <button  type="button"  onClick={() => this.viewSparepart(sparepart.id)} class="btn btn-info" >View More</button >
                                    </div>
                                    </div>
                                </div>
                                </div>
                        )
                    }
                    </div>
            </div>
            
        );
    }
}

export default Gallery;

