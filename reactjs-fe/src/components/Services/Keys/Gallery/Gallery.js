import React, { Component } from 'react';
// import "tabler-react/dist/Tabler.css";
// import { GalleryCard, Grid, Card } from 'tabler-react';

import { Button, Form, FormControl, NavDropdown, Nav, Navbar  } from 'react-bootstrap';

import './Gallery.css';
import KeyService from './../../../../api-request-general/KeyService';
import history from './../../../../history';

class Gallery extends Component {

    constructor(props){
        super(props);

        this.state={
            keys: []
        }

        this.viewKey=this.viewKey.bind(this);
    }

    viewKey(id)
    {
        history.push(`/view-key/${id}`);
    }

    componentDidMount(){
        KeyService.getKeys().then((res) =>{
            this.setState({keys:res.data});
        });
    }

    render() {
        return (
            
                
                <div className="card-group">
                    <div class="row">
                    {
                        this.state.keys.map(
                            key =>
                            <div class="col-3">
                                <div class="card">
                                    <img class="card-img-top" src={`data:image/jpeg;base64,${key.image}`}  alt="Card image cap"/>
                                    <div class="card-body">
                                    <h5 class="card-title"><div>{key.productCode}</div></h5>
                                    <p class="card-text">
                                        <label>Vehicle Brand: </label>
                                        <div>{key.vehicleBrand}</div>
                                    </p>
                                    <p class="card-text">
                                        
                                        Vehicle Type: {key.vehicleType}
                                    </p>
                                    <p class="card-text">
                                        <label><div>Price: {key.price}</div></label>
                                        
                                    </p>
                                    <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                                    <div class="btn-group" style={{width:"100%"}}>
                                    <button  type="button"  onClick={() => this.viewKey(key.id)} class="btn btn-info" >View More</button >
                                    </div>
                                    </div>
                                </div>
                                </div>
                        )
                    }
                    </div>
            </div>
            
        );
    }
}

export default Gallery;

