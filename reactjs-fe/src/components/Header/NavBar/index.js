import { Button, Form, FormControl, NavDropdown, Nav, Navbar  } from 'react-bootstrap';
import imag from "./../resources/logo_v1.png";
import './index.css';
import { withRouter } from 'react-router-dom';

import React, { Component } from 'react';
import history from '../../../history';

class FooterComponent extends Component {
    
    constructor(props){

        super(props);

        this.state={
            
        }

        this.about = this.about.bind(this);
        this.vehicles = this.vehicles.bind(this);
        this.contact = this.contact.bind(this);
        this.keys = this.keys.bind(this);
        this.spareparts = this.spareparts.bind(this);
        this.home = this.home.bind(this);
    }
    about(){
        // this.props.history.push('/about');
        history.push('/about');
    }
    vehicles(){
        // this.props.history.push('/services');
        history.push('/vehicles');
    }
    contact(){
        // this.props.history.push('/contact');
        history.push('/contact');
    }
    keys(){
        // this.props.history.push('/keys');
        history.push('/keys');
    }
    spareparts(){
        // this.props.history.push('/keys');
        history.push('/spareparts');
    }
    home(){
        // this.props.history.push('/keys');
        history.push('/');
    }
    render() {
        return (
            <div>
                <Navbar className="color-nav" variant="light" expand="lg">
                {/* <Navbar.Brand href="#home">React-Bootstrap</Navbar.Brand> */}
                <Navbar.Brand className="navbar-img" href="" onClick={() => this.home()}><img src={imag} alt="store" className="navbar-brand" /></Navbar.Brand>
                {/* <img src={imag} alt="store" className="navbar-brand" /> */}
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="mr-auto">
                        <Nav.Link onClick={() => this.home()} ><p className="textHead">Home</p></Nav.Link>
                        <Nav.Link onClick={() => this.about()} ><p className="textHead">About</p></Nav.Link>
                        <Nav.Link onClick={() => this.contact()} ><p className="textHead">Contact</p></Nav.Link>
                        <Nav.Link onClick={() => this.vehicles()} ><p className="textHead">Vehicles</p></Nav.Link>
                        <Nav.Link onClick={() => this.spareparts()} ><p className="textHead">Spareparts</p></Nav.Link>
                        <Nav.Link onClick={() => this.keys()} ><p className="textHead">Keys</p></Nav.Link>
                        {/* <NavDropdown title="Dropdown" id="basic-nav-dropdown">
                            <NavDropdown.Item href="#action/3.1">Action</NavDropdown.Item>
                            <NavDropdown.Item href="#action/3.2">Another action</NavDropdown.Item>
                            <NavDropdown.Item href="#action/3.3">Something</NavDropdown.Item>
                            <NavDropdown.Divider />
                            <NavDropdown.Item href="#action/3.4">Separated link</NavDropdown.Item>
                        </NavDropdown> */}
                    </Nav>

                    <Form inline>
                        <FormControl type="text" placeholder="Search" className="mr-sm-2" />
                        <Button variant="outline-success">Search</Button>
                    </Form>
                </Navbar.Collapse>
                </Navbar>
            </div>
        );
    }
}

export default FooterComponent;