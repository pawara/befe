import axios from 'axios';

const VEHICLE_API_BASE_URL = 'http://localhost:8080/api/v1/vehicles';
// const UPLOAD_IMAGE = "UPLOAD_IMAGE";

class VehicleService{

    getVehicles(){
        return axios.get(VEHICLE_API_BASE_URL);
    }

    createVehicle(vehicle)
    {
        return axios.post(VEHICLE_API_BASE_URL, vehicle);

        // return axios.post(KEY_API_BASE_URL,
        // employee,
        //         {
        //             headers: {
        //                 // "Authorization": "YOUR_API_AUTHORIZATION_KEY_SHOULD_GOES_HERE_IF_HAVE",
        //                 "Content-type": "multipart/form-data",
        //             },                    
        //         }
        // )
    }

 

    getVehicleById(vehicleId)
    {
        return axios.get(VEHICLE_API_BASE_URL+'/'+vehicleId);
    }

    //http://localhost:8080/api/v1/employees/2
    updateVehicle(vehicle, vehicleId)
    {
        return axios.put(VEHICLE_API_BASE_URL+'/'+vehicleId, vehicle); //returns a promise object
    }

    deleteVehicle(vehicleId)
    {
        //method to make a resp api call
        return axios.delete(VEHICLE_API_BASE_URL+'/'+vehicleId);
    }
}

export default new VehicleService()