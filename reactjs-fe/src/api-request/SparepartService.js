import axios from 'axios';

const SPAREPART_API_BASE_URL = 'http://localhost:8080/api/v1/spareparts';
// const UPLOAD_IMAGE = "UPLOAD_IMAGE";

class SparepartService{

    getSpareparts(){
        return axios.get(SPAREPART_API_BASE_URL);
    }

    createSparepart(sparepart)
    {
        return axios.post(SPAREPART_API_BASE_URL, sparepart);

        // return axios.post(KEY_API_BASE_URL,
        // employee,
        //         {
        //             headers: {
        //                 // "Authorization": "YOUR_API_AUTHORIZATION_KEY_SHOULD_GOES_HERE_IF_HAVE",
        //                 "Content-type": "multipart/form-data",
        //             },                    
        //         }
        // )
    }

 

    getSparepartById(sparepartId)
    {
        return axios.get(SPAREPART_API_BASE_URL+'/'+sparepartId);
    }

    //http://localhost:8080/api/v1/employees/2
    updateSparepart(sparepart, sparepartId)
    {
        return axios.put(SPAREPART_API_BASE_URL+'/'+sparepartId, sparepart); //returns a promise object
    }

    deleteSparepart(sparepartId)
    {
        //method to make a resp api call
        return axios.delete(SPAREPART_API_BASE_URL+'/'+sparepartId);
    }
}

export default new SparepartService()