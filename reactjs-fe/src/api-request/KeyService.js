import axios from 'axios';

const KEY_API_BASE_URL = 'http://localhost:8080/api/v1/keys';
// const UPLOAD_IMAGE = "UPLOAD_IMAGE";

class KeyService{

    getKeys(){
        return axios.get(KEY_API_BASE_URL);
    }

    createKey(key)
    {
        return axios.post(KEY_API_BASE_URL, key);

        // return axios.post(KEY_API_BASE_URL,
        // employee,
        //         {
        //             headers: {
        //                 // "Authorization": "YOUR_API_AUTHORIZATION_KEY_SHOULD_GOES_HERE_IF_HAVE",
        //                 "Content-type": "multipart/form-data",
        //             },                    
        //         }
        // )
    }

 

    getKeyById(keyId)
    {
        return axios.get(KEY_API_BASE_URL+'/'+keyId);
    }

    //http://localhost:8080/api/v1/employees/2
    updateKey(key, keyId)
    {
        return axios.put(KEY_API_BASE_URL+'/'+keyId, key); //returns a promise object
    }

    deleteKey(keyId)
    {
        //method to make a resp api call
        return axios.delete(KEY_API_BASE_URL+'/'+keyId);
    }
}

export default new KeyService()