import axios from 'axios';

const SPAREPART_API_BASE_URL = 'http://localhost:8080/api/v1/spareparts';
// const UPLOAD_IMAGE = "UPLOAD_IMAGE";

class SparepartService{

    getSpareparts(){
        return axios.get(SPAREPART_API_BASE_URL);
    }

    getSparepartById(sparepartId)
    {
        return axios.get(SPAREPART_API_BASE_URL+'/'+sparepartId);
    }
}

export default new SparepartService()