import axios from 'axios';

const KEY_API_BASE_URL = 'http://localhost:8080/api/v1/keys';
// const UPLOAD_IMAGE = "UPLOAD_IMAGE";

class KeyService{

    getKeys(){
        return axios.get(KEY_API_BASE_URL);
    }

    getKeyById(keyId)
    {
        return axios.get(KEY_API_BASE_URL+'/'+keyId);
    }


}

export default new KeyService()