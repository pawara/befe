import axios from 'axios';

const VEHICLE_API_BASE_URL = 'http://localhost:8080/api/v1/vehicles';
// const UPLOAD_IMAGE = "UPLOAD_IMAGE";

class VehicleService{

    getVehicles(){
        return axios.get(VEHICLE_API_BASE_URL);
    }

    getVehicleById(vehicleId)
    {
        return axios.get(VEHICLE_API_BASE_URL+'/'+vehicleId);
    }
}

export default new VehicleService()