package com.aus.sl.exception;

public class KeyNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public KeyNotFoundException(String message) {
        super(message);
    }

    public KeyNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
