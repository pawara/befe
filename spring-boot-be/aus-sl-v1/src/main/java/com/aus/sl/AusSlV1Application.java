package com.aus.sl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AusSlV1Application {

	public static void main(String[] args) {
		SpringApplication.run(AusSlV1Application.class, args);
	}
}
