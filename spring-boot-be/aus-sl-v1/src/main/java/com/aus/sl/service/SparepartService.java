package com.aus.sl.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.aus.sl.exception.SparepartNotFoundException;
import com.aus.sl.exception.SparepartStorageException;
import com.aus.sl.model.Sparepart;
import com.aus.sl.repository.SparepartRepository;

//import com.keyhut.springboot.exception.FileNotFoundException;
//import com.keyhut.springboot.exception.FileStorageException;
//import com.keyhut.springboot.model.Employee;
//import com.keyhut.springboot.repository.DatabaseFileRepository;

@Service
public class SparepartService {
	@Autowired
    private SparepartRepository sparepartRepository;
	
	public Sparepart insertSparepart(
			String product_code, 
			String vehicle_brand,
			String vehicle_type, 
			String sparepart_name, 
			int quantity, int price, MultipartFile image)
	{
		try 
		{
			Sparepart sparepart = new Sparepart(product_code, vehicle_brand, vehicle_type, sparepart_name, quantity, price,
					image.getBytes());
			
			return sparepartRepository.save(sparepart);
			
		}catch (IOException ex) {
            throw new SparepartStorageException("Could not store sparepart " + product_code + ". Please try again!", ex);
        }
	}

	public List<Sparepart> getAllSpareparts()
	{
		return sparepartRepository.findAll();
	}
	
	
	public ResponseEntity<Sparepart> getSparepartById(String id) 
	{
		Sparepart sparepart = sparepartRepository.findById(id).orElseThrow(
				() -> new SparepartNotFoundException("Sparepart Not Exist with id: "+id)
			);
		return ResponseEntity.ok(sparepart);
	}
	
	public ResponseEntity<Sparepart> updateSparepart(String id, String product_code, String vehicle_brand,String vehicle_type, String sparepart_name, int quantity, int price, MultipartFile image)
	{
		Sparepart sparepart = sparepartRepository.findById(id).orElseThrow(
				() -> new SparepartNotFoundException("Sparepart Not Exist with id: "+id));
		
//		String key = StringUtils.cleanPath(file.getOriginalFilename());
		
		sparepart.setId(id);
		sparepart.setPrice(price);
		sparepart.setProductCode(product_code);
		sparepart.setQuantity(quantity);
		sparepart.setSparepartName(sparepart_name);
		sparepart.setVehicleBrand(vehicle_brand);
		sparepart.setVehicleType(vehicle_type);
		
		
		try {
			sparepart.setImage(image.getBytes());
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		Sparepart updatedSparepart  = sparepartRepository.save(sparepart);
		return ResponseEntity.ok(updatedSparepart);
	}
	
	public ResponseEntity<Map<String, Boolean>> deleteSparepart(String id)
	{
		Sparepart sparepart = sparepartRepository.findById(id).orElseThrow(
				() -> new SparepartNotFoundException("Sparepart Not Exist with id: "+id));
		sparepartRepository.delete(sparepart);
		
		Map<String, Boolean> response = new HashMap<>();
		response.put("deleted", Boolean.TRUE);
		return ResponseEntity.ok(response);
	}
  
}
