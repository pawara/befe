package com.aus.sl.exception;

public class KeyStorageException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;

	public KeyStorageException(String message) {
        super(message);
    }

    public KeyStorageException(String message, Throwable cause) {
        super(message, cause);
    }
}