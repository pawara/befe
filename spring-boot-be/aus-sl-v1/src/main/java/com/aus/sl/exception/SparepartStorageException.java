package com.aus.sl.exception;

public class SparepartStorageException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;

	public SparepartStorageException(String message) {
        super(message);
    }

    public SparepartStorageException(String message, Throwable cause) {
        super(message, cause);
    }
}