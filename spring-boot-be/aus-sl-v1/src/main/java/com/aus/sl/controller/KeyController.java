package com.aus.sl.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
//
//import com.aus.sl.model.Employee;
//import com.keyhut.springboot.payload.Response;
//import com.keyhut.springboot.service.DatabaseFileService;

import com.aus.sl.model.Key;
import com.aus.sl.service.KeyService;


import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

//import com.keyhut.springboot.model.Employee;
//import com.keyhut.springboot.payload.Response;
//import com.keyhut.springboot.service.DatabaseFileService;


@CrossOrigin(origins = "http://localhost:3012")
@RestController
@RequestMapping("/api/v1/")
public class KeyController {

	@Autowired
    private KeyService keyService;
	
    @PostMapping("/keys")
    public Key createKey(
    		@RequestParam("image") MultipartFile image, 
    		@RequestParam("product_code") String product_code,
    		@RequestParam("vehicle_brand") String vehicle_brand,
    		@RequestParam("vehicle_type") String vehicle_type,
    		@RequestParam("quantity") int quantity,
    		@RequestParam("price") int price) 
    {
    	
    	System.out.println("TEST => product_code : "+product_code);
    	Key key = keyService.insertKey(product_code, vehicle_brand,vehicle_type, quantity, price, image);
    	return key;
    }
    
	@GetMapping("/keys")
	public List<Key> getAllKeys()
	{
		System.out.println("TEST => getAllKeys 123: "+ keyService.getAllKeys().size());
		
		return keyService.getAllKeys();
	}
	
	// get employee by id rest api
	@GetMapping("/keys/{id:.+}")
	public ResponseEntity<Key> getKeyById(@PathVariable String id, HttpServletRequest request)
	{
    	ResponseEntity<Key> key = keyService.getKeyById(id);

    	return key;
				
		
	}
	
	@PutMapping("/keys/{id}")
	public ResponseEntity<Key> updateKey(
			@PathVariable String id,
			@RequestParam("image") MultipartFile image, 
    		@RequestParam("product_code") String product_code,
    		@RequestParam("vehicle_brand") String vehicle_brand,
    		@RequestParam("vehicle_type") String vehicle_type,
    		@RequestParam("quantity") int quantity,
    		@RequestParam("price") int price)
	{
		return keyService.updateKey(id, product_code, vehicle_brand,vehicle_type, quantity, price, image);
	}
	
	@DeleteMapping("/keys/{id}")
	public ResponseEntity<Map<String, Boolean>> deleteKey(@PathVariable String id)
	{
		return keyService.deleteKey(id);
	}
}
