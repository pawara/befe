package com.aus.sl.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
//
//import com.aus.sl.model.Employee;
//import com.keyhut.springboot.payload.Response;
//import com.keyhut.springboot.service.DatabaseFileService;

import com.aus.sl.model.Sparepart;
import com.aus.sl.service.SparepartService;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

//import com.keyhut.springboot.model.Employee;
//import com.keyhut.springboot.payload.Response;
//import com.keyhut.springboot.service.DatabaseFileService;


@CrossOrigin(origins = "http://localhost:3012")
@RestController
@RequestMapping("/api/v1/")
public class SparepartController {

	@Autowired
    private SparepartService sparepartService;
	
    @PostMapping("/spareparts")
    public Sparepart createSparepart(
    		@RequestParam("image") MultipartFile image, 
    		@RequestParam("product_code") String product_code,
    		@RequestParam("vehicle_brand") String vehicle_brand,
    		@RequestParam("vehicle_type") String vehicle_type,
    		@RequestParam("sparepart_name") String sparepart_name,
    		@RequestParam("quantity") int quantity,
    		@RequestParam("price") int price) 
    {
    	
    	System.out.println("TEST => product_code : "+product_code);
    	Sparepart sparepart = sparepartService.insertSparepart(product_code, vehicle_brand,vehicle_type, sparepart_name, quantity, price, image);
    	return sparepart;
    }
    
	@GetMapping("/spareparts")
	public List<Sparepart> getAllSpareparts()
	{
		System.out.println("TEST => getAllSpareparts 123: "+ sparepartService.getAllSpareparts().size());
		
		return sparepartService.getAllSpareparts();
	}
	
	// get employee by id rest api
	@GetMapping("/spareparts/{id:.+}")
	public ResponseEntity<Sparepart> getSparepartById(@PathVariable String id, HttpServletRequest request)
	{
    	ResponseEntity<Sparepart> sparepart = sparepartService.getSparepartById(id);

    	return sparepart;
				
		
	}
	
	@PutMapping("/spareparts/{id}")
	public ResponseEntity<Sparepart> updateSparepart(
			@PathVariable String id,
			@RequestParam("image") MultipartFile image, 
    		@RequestParam("product_code") String product_code,
    		@RequestParam("vehicle_brand") String vehicle_brand,
    		@RequestParam("vehicle_type") String vehicle_type,
    		@RequestParam("sparepart_name") String sparepart_name,
    		@RequestParam("quantity") int quantity,
    		@RequestParam("price") int price)
	{
		return sparepartService.updateSparepart(id, product_code, vehicle_brand, vehicle_type, sparepart_name, quantity, price, image);
	}
	
	@DeleteMapping("/spareparts/{id}")
	public ResponseEntity<Map<String, Boolean>> deleteSparepart(@PathVariable String id)
	{
		return sparepartService.deleteSparepart(id);
	}
	
}
