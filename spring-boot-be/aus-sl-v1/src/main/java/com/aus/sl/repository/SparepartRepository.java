package com.aus.sl.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.aus.sl.model.Key;
import com.aus.sl.model.Sparepart;

@Repository
public interface SparepartRepository extends JpaRepository<Sparepart, String> {

}
