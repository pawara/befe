package com.aus.sl.model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "vehicle_keys")
public class Key {
	
	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	private String id;
	
	public Key() {
		super();
	}

	public Key(String id, String productCode, String vehicleBrand, String vehicleType, int quantity, int price,
			byte[] image) {
		super();
		this.id = id;
		this.productCode = productCode;
		this.vehicleBrand = vehicleBrand;
		this.vehicleType = vehicleType;
		this.quantity = quantity;
		this.price = price;
		this.image = image;
	}
	
	public Key(String id, String productCode, String vehicleBrand, String vehicleType, int quantity, int price) {
		super();
		this.id = id;
		this.productCode = productCode;
		this.vehicleBrand = vehicleBrand;
		this.vehicleType = vehicleType;
		this.quantity = quantity;
		this.price = price;
	}

	public Key(String productCode, String vehicleBrand, String vehicleType, int quantity, int price,
			byte[] image) {
		super();
		this.productCode = productCode;
		this.vehicleBrand = vehicleBrand;
		this.vehicleType = vehicleType;
		this.quantity = quantity;
		this.price = price;
		this.image = image;
	}

	@Column(name="product_code")
	private String productCode;
	
	@Column(name="vehicle_brand")
	private String vehicleBrand;
	
	@Column(name="vehicle_type")
	private String vehicleType;

	@Column(name="quantity")
	private int quantity;

	@Column(name="price")
	private int price;

	@Lob
	private byte[] image;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getVehicleBrand() {
		return vehicleBrand;
	}

	public void setVehicleBrand(String vehicleBrand) {
		this.vehicleBrand = vehicleBrand;
	}

	public String getVehicleType() {
		return vehicleType;
	}

	public void setVehicleType(String vehicleType) {
		this.vehicleType = vehicleType;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public byte[] getImage() {
		return image;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}
	
}
