package com.aus.sl.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;


import com.aus.sl.exception.VehicleNotFoundException;
import com.aus.sl.exception.VehicleStorageException;
import com.aus.sl.model.Vehicle;
import com.aus.sl.repository.VehicleRepository;

//import com.keyhut.springboot.exception.FileNotFoundException;
//import com.keyhut.springboot.exception.FileStorageException;
//import com.keyhut.springboot.model.Employee;
//import com.keyhut.springboot.repository.DatabaseFileRepository;

@Service
public class VehicleService {
	@Autowired
    private VehicleRepository vehicleRepository;
	
	public Vehicle insertVehicle(
			String product_code, 
			String vehicle_brand,
			String vehicle_type, 
			int quantity, 
			int price, 
			MultipartFile image)
	{
		try 
		{
			Vehicle vehicle = new Vehicle(product_code, vehicle_brand, vehicle_type, quantity, price,
					image.getBytes());
			return vehicleRepository.save(vehicle);
		}catch (IOException ex) {
            throw new VehicleStorageException("Could not store vehicle " + product_code + ". Please try again!", ex);
        }
	}

	public List<Vehicle> getAllVehicles()
	{
		return vehicleRepository.findAll();
	}
	
	
	public ResponseEntity<Vehicle> getVehicleById(String id) 
	{
		Vehicle vehicle = vehicleRepository.findById(id).orElseThrow(
				() -> new VehicleNotFoundException("Vehicle Not Exist with id: "+id)
			);
		return ResponseEntity.ok(vehicle);
	}
	
	public ResponseEntity<Vehicle> updateVehicle(
			String id, 
			String product_code, 
			String vehicle_brand,
			String vehicle_type, 
			int quantity, 
			int price, 
			MultipartFile image)
	{
		Vehicle vehicle = vehicleRepository.findById(id).orElseThrow(
				() -> new VehicleNotFoundException("Vehicle Not Exist with id: "+id));
		
//		String key = StringUtils.cleanPath(file.getOriginalFilename());
		
		vehicle.setId(id);
		vehicle.setPrice(price);
		vehicle.setProductCode(product_code);
		vehicle.setQuantity(quantity);
		vehicle.setVehicleBrand(vehicle_brand);
		vehicle.setVehicleType(vehicle_type);
		
		
		try {
			vehicle.setImage(image.getBytes());
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		Vehicle updatedVehicle  = vehicleRepository.save(vehicle);
		return ResponseEntity.ok(updatedVehicle);
	}
	
	public ResponseEntity<Map<String, Boolean>> deleteVehicle(String id)
	{
		Vehicle vehicle = vehicleRepository.findById(id).orElseThrow(
				() -> new VehicleNotFoundException("Vehicle Not Exist with id: "+id));
		vehicleRepository.delete(vehicle);
		
		Map<String, Boolean> response = new HashMap<>();
		response.put("deleted", Boolean.TRUE);
		return ResponseEntity.ok(response);
	}
  
}
