package com.aus.sl.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.aus.sl.exception.KeyNotFoundException;
import com.aus.sl.exception.KeyStorageException;
import com.aus.sl.model.Key;
import com.aus.sl.repository.KeyRepository;

//import com.keyhut.springboot.exception.FileNotFoundException;
//import com.keyhut.springboot.exception.FileStorageException;
//import com.keyhut.springboot.model.Employee;
//import com.keyhut.springboot.repository.DatabaseFileRepository;

@Service
public class KeyService {
	@Autowired
    private KeyRepository keyRepository;
	
	public Key insertKey(
			String product_code, 
			String vehicle_brand,
			String vehicle_type, 
			int quantity, int price, MultipartFile image)
	{
		try 
		{
			Key key = new Key(product_code, vehicle_brand, vehicle_type, quantity, price,
					image.getBytes());
			return keyRepository.save(key);
		}catch (IOException ex) {
            throw new KeyStorageException("Could not store key " + product_code + ". Please try again!", ex);
        }
	}

	public List<Key> getAllKeys()
	{
		return keyRepository.findAll();
	}
	
	
	public ResponseEntity<Key> getKeyById(String id) 
	{
		Key key = keyRepository.findById(id).orElseThrow(
				() -> new KeyNotFoundException("Key Not Exist with id: "+id)
			);
		return ResponseEntity.ok(key);
	}
	
	public ResponseEntity<Key> updateKey(String id, String product_code, String vehicle_brand,String vehicle_type, int quantity, int price, MultipartFile image)
	{
		Key key = keyRepository.findById(id).orElseThrow(
				() -> new KeyNotFoundException("Key Not Exist with id: "+id));
		
//		String key = StringUtils.cleanPath(file.getOriginalFilename());
		
		key.setId(id);
		key.setPrice(price);
		key.setProductCode(product_code);
		key.setQuantity(quantity);
		key.setVehicleBrand(vehicle_brand);
		key.setVehicleType(vehicle_type);
		
		
		try {
			key.setImage(image.getBytes());
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		Key updatedKey  = keyRepository.save(key);
		return ResponseEntity.ok(updatedKey);
	}
	
	public ResponseEntity<Map<String, Boolean>> deleteKey(String id)
	{
		Key key = keyRepository.findById(id).orElseThrow(
				() -> new KeyNotFoundException("Key Not Exist with id: "+id));
		keyRepository.delete(key);
		
		Map<String, Boolean> response = new HashMap<>();
		response.put("deleted", Boolean.TRUE);
		return ResponseEntity.ok(response);
	}
  
}
