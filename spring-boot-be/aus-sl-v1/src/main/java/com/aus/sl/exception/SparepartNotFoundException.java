package com.aus.sl.exception;

public class SparepartNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public SparepartNotFoundException(String message) {
        super(message);
    }

    public SparepartNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
