package com.aus.sl.exception;

public class VehicleStorageException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;

	public VehicleStorageException(String message) {
        super(message);
    }

    public VehicleStorageException(String message, Throwable cause) {
        super(message, cause);
    }
}