package com.aus.sl.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.aus.sl.model.Key;

@Repository
public interface KeyRepository extends JpaRepository<Key, String> {

}
