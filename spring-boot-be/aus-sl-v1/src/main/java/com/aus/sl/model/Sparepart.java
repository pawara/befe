package com.aus.sl.model;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "spareparts")
public class Sparepart {
	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	private String id;
	
	@Column(name="product_code")
	private String productCode;
	
	@Column(name="Vehicle_brand")
	private String VehicleBrand;
	
	@Column(name="vehicle_type")
	private String vehicleType;
	
	@Column(name="sparepart_name")
	private String sparepartName;

	@Column(name="quantity")
	private int quantity;

	@Column(name="price")
	private int price;

	@Lob
	private byte[] image;

	public Sparepart(String id, String productCode, String vehicleBrand, String vehicleType, String sparepartName,
			int quantity, int price, byte[] image) {
		super();
		this.id = id;
		this.productCode = productCode;
		VehicleBrand = vehicleBrand;
		this.vehicleType = vehicleType;
		this.sparepartName = sparepartName;
		this.quantity = quantity;
		this.price = price;
		this.image = image;
	}

	public Sparepart(String productCode, String vehicleBrand, String vehicleType, String sparepartName, int quantity,
			int price, byte[] image) {
		super();
		this.productCode = productCode;
		VehicleBrand = vehicleBrand;
		this.vehicleType = vehicleType;
		this.sparepartName = sparepartName;
		this.quantity = quantity;
		this.price = price;
		this.image = image;
	}

	public Sparepart() {

	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getVehicleBrand() {
		return VehicleBrand;
	}

	public void setVehicleBrand(String vehicleBrand) {
		VehicleBrand = vehicleBrand;
	}

	public String getVehicleType() {
		return vehicleType;
	}

	public void setVehicleType(String vehicleType) {
		this.vehicleType = vehicleType;
	}

	public String getSparepartName() {
		return sparepartName;
	}

	public void setSparepartName(String sparepartName) {
		this.sparepartName = sparepartName;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public byte[] getImage() {
		return image;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}
}
